import { initializeApp } from "firebase/compat/app";
import firebase from 'firebase/compat/app';
import 'firebase/compat/database'

const firebaseConfig = {
  apiKey: "AIzaSyD2mjeu8sqCFNdNyzp67pAu8HeK2Oe0YKc",
  authDomain: "react-crud-f8407.firebaseapp.com",
  databaseURL: "https://react-crud-f8407-default-rtdb.asia-southeast1.firebasedatabase.app",
  projectId: "react-crud-f8407",
  storageBucket: "react-crud-f8407.appspot.com",
  messagingSenderId: "988927799893",
  appId: "1:988927799893:web:7c6a4261c03b54eba79adc",
  measurementId: "G-0LSFCSTBZ7"
};

// Initialize Firebase
const firedb = firebase.initializeApp(firebaseConfig);
export default firedb.database().ref();
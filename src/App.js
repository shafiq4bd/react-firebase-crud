
import { result } from 'lodash';
import Container from '@mui/material/Container';
import Button from '@mui/material/Button';
import  styled  from 'styled-components'
import Contact from './components/Contact';
import Grid from '@mui/material/Grid';



function App() {
  

  return (
    <div className="App">
      <Contact />
    </div>
  );
}

export default App;

import Container from '@mui/material/Container';
import Grid from '@mui/material/Grid';
import Button from '@mui/material/Button';
import  styled  from 'styled-components'
import { useState } from 'react';
import { useEffect } from 'react';
import { FaRegEdit } from 'react-icons/fa';
import { AiFillDelete } from 'react-icons/ai';

import ContactForm from './ContactForm';
import firebasedb from "../firebase";

//table
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';

const HeaderArea = styled.div`
  display:flex;
  align-items:center;
  justify-content:space-between;
  margin-top:50px;
`;
const ContentArea = styled.div`
  margin-top:30px;
  & .edit-btn{
    margin-right:10px;
  }
`;

const Contact = ()=>{
  
  const [contactObj, setContactObj] = useState({});
  const [editId, setEditId] = useState('');

  useEffect(()=>{
    firebasedb.child('contacts').on('value', snapshot =>{
      let list = snapshot.val();
      if(list !=null){
        setContactObj({...list});
      }
    })
  }, []);

  const addOrEdit = obj => {
    if(editId == ''){
      firebasedb.child('contacts').push(
        obj,
        (err)=>{
          if(err){
            console.log(err);
          }else{
            setEditId(null);
            console.log("success");
           
          }
        }
      )
      
    }else{
      firebasedb.child(`contacts/${editId}`).set(
        obj,
        (err)=>{
          if(err){
            console.log(err);
          }else{
            setEditId('');
            console.log("Updated");
           
          }
        }
      )
    }

    }
    const deleteContact = key =>{
      if(window.confirm("Are you want to delete contact?")){
        firebasedb.child(`contacts/${key}`).remove(
          (err)=>{
            if(err){
              console.log(err);
            }else{
              setEditId('');
              console.log("Deleted");
             
            }
          }
        )
      }
    }
    
  
  return(
   <>
      <Container maxWidth="lg">
            <HeaderArea>
              <h4>AYR Admin panel</h4>
              <Button variant="contained" >
                Create User
              </Button>
            </HeaderArea>  
          <ContentArea>
            <Grid container spacing={3}>
            
            <Grid item md={4}>
              <ContactForm {...({addOrEdit, editId, contactObj})}/>
            </Grid>
            <Grid item md={8}>
              <TableContainer component={Paper}>
                <Table  aria-label="simple table">
                  <TableHead>
                    <TableRow>
                      <TableCell>Name</TableCell>
                      <TableCell align="left">Email</TableCell>
                      <TableCell align="left">Age</TableCell>
                      <TableCell align="left">Mobile</TableCell>
                      <TableCell align="left">Address</TableCell>
                      <TableCell align="center">Action</TableCell>
                      
                    </TableRow>
                  </TableHead>
                  <TableBody>
                    {
                    Object.keys(contactObj).map((contact, index) => (
                    
                      <TableRow
                      key={index}
                        sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
                      >
                        <TableCell align="left">{contactObj[contact].name}</TableCell>
                        <TableCell align="left">{contactObj[contact].email}</TableCell>
                        <TableCell align="left">{contactObj[contact].age}</TableCell>
                        <TableCell align="left">{contactObj[contact].mobile}</TableCell>
                        <TableCell align="left">{contactObj[contact].address}</TableCell>
                        <TableCell align="center"> 
                        <Button  variant="contained" type="submit" 
                        onClick = {()=> setEditId(contact)} className="edit-btn" color="success"> <FaRegEdit /> </Button>
                        <Button variant="contained"  onClick = {()=> deleteContact(contact)} color="error"> <AiFillDelete /></Button>
                        </TableCell>
                        
                      </TableRow>
                      
                    ))}
                  </TableBody>
                </Table>
              </TableContainer>
            </Grid>
          </Grid>
          </ContentArea>
    
      </Container>



    
   </>

  );
}
export default Contact;
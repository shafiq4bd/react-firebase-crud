import Container from "@mui/material/Container";
import Grid from "@mui/material/Grid";
import styled from "styled-components";
import TextField from "@mui/material/TextField";
import Button from "@mui/material/Button";
import { useState } from "react";
import { useEffect } from "react";

const Input = styled(TextField)`
  margin-bottom: 10px;

  width: 100% !important;
`;
const SubmitButton = styled(Button)``;
const ButtonWrap = styled.div`
  text-align: right;
`;

const ContactForm = (props) => {
  const initialFieldValues = {
    name: null,
    email: null,
    mobile: null,
    age: null,
    address: null,
  };

  const [contacts, setContacts] = useState(initialFieldValues);
  const handleInputChange = (e) => {
    let { name, value } = e.target;
    setContacts({
      ...contacts,
      [name]: value,
    });
  };

  useEffect(() => {
    debugger;
    if (props.editId == null) {
      setContacts({ ...initialFieldValues });
    } else {
      setContacts({ ...props.contactObj[props.editId] });
      console.log(contacts);
    }
  }, [props.editId, props.contactObj]);

  const handleFormSubmit = (e) => {
    e.preventDefault();
    props.addOrEdit(contacts);

    // e.target.reset();
  };

  return (
    <>
      <form autoComplete="off" onSubmit={handleFormSubmit}>
        <Input
          id="name"
          label="name"
          variant="outlined"
          autoComplete="off"
          name="name"
          value={contacts.name ? contacts.name : ""}
          onChange={handleInputChange}
        />
        <Input
          id="email"
          label="Email"
          variant="outlined"
          autoComplete="off"
          name="email"
          value={contacts.email ? contacts.email : ""}
          onChange={handleInputChange}
        />
        <Input
          id="mobile"
          label="Mobile"
          variant="outlined"
          autoComplete="off"
          name="mobile"
          value={contacts.mobile ? contacts.mobile : ""}
          onChange={handleInputChange}
        />
        <Input
          id="age"
          label="Age"
          variant="outlined"
          autoComplete="off"
          name="age"
          value={contacts.age ? contacts.age : ""}
          onChange={handleInputChange}
        />
        <Input
          id="address"
          label="Address"
          variant="outlined"
          autoComplete="off"
          name="address"
          value={contacts.address ? contacts.address : ""}
          onChange={handleInputChange}
        />

        <ButtonWrap>
          <SubmitButton variant="contained" type="submit">
            {" "}
            {props.editId ? "Update" : "Save"}
          </SubmitButton>
        </ButtonWrap>
      </form>
    </>
  );
};
export default ContactForm;
